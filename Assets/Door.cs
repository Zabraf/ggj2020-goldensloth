﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    private GameObject ForceField;
    private GameObject LightForceField;
    public Environement envi;
    // Start is called before the first frame update
    void Start()
    {
        ForceField = GameObject.Find("ChampsDeForce");
        LightForceField = GameObject.Find("ChampsDeForceLight");
        ForceField.SetActive(false);
        LightForceField.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        print(collision.tag);
        if(collision.tag == "Player")
        {
            ForceField.SetActive(true);
            LightForceField.SetActive(true);
            envi.StartTimer();
        }
    }
}
