﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class UIGame : MonoBehaviour
{
    //The pannel of the game over
    public GameObject gameOverPannel;
    //The pannel of a successful run
    public GameObject winPannel;
    //The label showing the time left
    public TMP_Text timeLeftLbl;
    //The label of the current item repaired and Total
    public TMP_Text completionLbl;
    //The label of the gameover screen
    public TMP_Text DeathMessage;
    /// <summary>
    /// Make sure the gamover pannel is deactivated at the start
    /// </summary>
    private void Awake()
    {
        gameOverPannel.SetActive(false);
    }
    /// <summary>
    /// Update the label of completion with the new values
    /// </summary>
    /// <param name="broken"></param>
    /// <param name="total"></param>
    public void Completion(int broken, int total) {
        completionLbl.text = broken + "/" + total;
    }
    /// <summary>
    /// Activate or deactivate the pannel of the gameover
    /// </summary>
    public void GameOverPannel(int codeDeath) {
        gameOverPannel.SetActive(!gameOverPannel.activeSelf);
        switch (codeDeath)
        {
            case 0:
                DeathMessage.text = InsultGenerator(0);
                break;
            case 1:
                DeathMessage.text = InsultGenerator(1); 
                break;
        }
    }
    /// <summary>
    /// Activate or deactivate the pannel of the winning pannel
    /// </summary>
    public void WinPannel() {
        winPannel.SetActive(!gameOverPannel.activeSelf);
    }
    /// <summary>
    /// Load the next level
    /// </summary>
    public void NextLevel() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
    }
    /// <summary>
    /// Quit the game to the main menu
    /// </summary>
    public void ExitGame() {
        SceneManager.LoadScene(0);
    }
    /// <summary>
    /// Reload the same level
    /// </summary>
    public void tryAgain() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    /// <summary>
    /// It not a bug its a feature
    /// </summary>
    /// <param name="deathType"></param>
    /// <returns></returns>
    private string InsultGenerator(int deathType) {
        if (deathType == 0)
        {
            List<string> insults = new List<string>() { "Too late... \nthe ship did not whistand the criticals failure, but his worst failure in the end was you...",
                                                        "You failed but honestly what else did you expect ?",
                                                        "You failed at keeping a timely schedule work",
                                                        "Too late... \nas always..."};

            return insults[Random.Range(0, insults.Count)];
        }
        else {
            List<string> insults = new List<string>() {"Pro tip: Monster =/= hug",
                                                        "Is it the kind glowing eyes or the lovely embrace that brought you here ? ",
                                                         "You DIED",
                                                        "The premature death by monster is no reason to not do ones job",
                                                        "Only death awaited you here anyway"};
            return insults[Random.Range(0, insults.Count)];
        }
    }
}
