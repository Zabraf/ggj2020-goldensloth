﻿using UnityEngine;

enum entites
{
    Fantome,
    Bat
}
public class Monster : MonoBehaviour
{
    [SerializeField]
    private int radius;
    [SerializeField]
    private int speed;
    [SerializeField]
    private Vector2 destination;
    [SerializeField]
    private entites entitie;
    private Player _player;
    private Animator Annima;
    private bool isIdle;
    private float Marge = 0.5f;
    // Start is called before the first frame update
    void Start()
    {
        _player = FindObjectOfType<Player>();
        destination = transform.position;
        Annima = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    { 
        float step = speed * Time.deltaTime;
        transform.position = Vector2.MoveTowards(transform.position, destination, step);
        float angle = Mathf.Atan2(_player.transform.position.y - transform.position.y, _player.transform.position.x - transform.position.x);
        if(destination.x < transform.position.x + Marge && destination.y < transform.position.y + Marge && destination.x > transform.position.x - Marge && destination.y > transform.position.y - Marge)
        {
            isIdle = true;
        }
        else
        {
            isIdle = false;
        }
        Annima.SetFloat("FloatX", Mathf.Cos(angle));
        Annima.SetFloat("FloatY", Mathf.Sin(angle));
        Annima.SetBool("Idle", isIdle);
        if (Mathf.Sqrt(Mathf.Pow(_player.transform.position.x, 2) + Mathf.Pow(_player.transform.position.y, 2)) <= radius)
        {
            if (_player.TooFast || _player.IsFixing)
            {
                RaycastHit2D hit = Physics2D.Raycast(transform.position , _player.transform.position - transform.position);
                
                Debug.DrawRay(transform.position, _player.transform.position - transform.position, Color.red);
                if (hit.transform.tag == "Player" || entitie == entites.Fantome)
                {
                    destination = new Vector2(_player.transform.position.x, _player.transform.position.y);
                }
            }
        }
    }
}
