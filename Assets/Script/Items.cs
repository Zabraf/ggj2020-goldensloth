﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The three diferent states of the objects
/// </summary>
public enum States
{
    isBroken,
    fixing,
    fix,
}
public class Items : MonoBehaviour
{

    public string nom;
    public Animator anima;
    public float TimeLeft;
    private States state;
    public Sprite broken;
    public Sprite unbroken;

    public Environement boss;
    /// <summary>
    /// Getter setter of the states to change their behavior depending on the state
    /// </summary>
    public States State
    {
        get
        {

            return state;
        }

        set
        {
            switch (value)
            {
                case States.isBroken:
                    GetComponent<SpriteRenderer>().sprite = broken;
                    StopAllCoroutines();
                    break;
                case States.fixing:
                    break;
                case States.fix:
                    GetComponent<SpriteRenderer>().sprite = unbroken;
                    boss.NumberObjectFixed++;
                    break;
                default:
                    break;
            }
            state = value;

        }
    }

    /// <summary>
    /// Get the animator from the gameobject
    /// </summary>
    void Start()
    {
        anima = GetComponentInChildren<Animator>();
    }
    /// <summary>
    /// Change the state of the game and start the timer of reparation
    /// </summary>
    public void Reparation()
    {
        if (state!=States.fix)
        {
            StartCoroutine(TimerAction());
        }
        
    }
    /// <summary>
    /// change the state of the item and stop the timer of reparation
    /// </summary>
    public void StopReperation()
    {
        if (state!=States.fix)
        {
            ChangeState(0);
        }
        StopCoroutine(TimerAction());
    }
    /// <summary>
    /// Coroutine of the Timer save the current value in a global variable to keep the time in case of stoping reparation
    /// </summary>
    /// <returns></returns>
    IEnumerator TimerAction()
    {
        ChangeState(1);
        float timer = 1f;
        for (float i = TimeLeft; i > 0; i = i - timer)
        {
            TimeLeft = i;
            yield return new WaitForSeconds(timer);
        }
        State = States.fix;
    }
    /// <summary>
    /// Switch for changing the state in simple comand and from ext scripts
    /// </summary>
    /// <param name="v"></param>
    internal void ChangeState(int v)
    {
        switch (v)
        {
            default:
                State = States.isBroken;
                break;
            case 0:
                State = States.isBroken;
                break;
            case 1:
                State = States.fixing;
                break;
            case 2:
                State = States.fix;
                break;
        }
    }
}
