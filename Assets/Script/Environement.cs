﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class Environement : MonoBehaviour
{
    private int _timer;
    public List<Items> _items;
    private List<Vector2> allSpawnPoints;
    public int numberObjectFixed=0;
    public int NumberObjectTotal;
    private int percetageOfActiveMachines;

    public int TotalTime;

    public UIGame gameUI;

    bool changingScore=false;

    public int NumberObjectFixed 
    {
        get
        {
            return numberObjectFixed;
        }

        set
        {
            if (!changingScore)
            {
                
                StartCoroutine(BufferInput());
                numberObjectFixed = value;
                if (value != 0)
                {
                    gameUI.Completion(numberObjectFixed, NumberObjectTotal);
                    if (numberObjectFixed == NumberObjectTotal)
                    {
                        StopTimer();
                        gameUI.WinPannel();
                    }

                }

            }
            
            
        }
    }

    IEnumerator BufferInput() {
        changingScore = true;
        yield return new WaitForSeconds(2);
        changingScore = false;
    }
    
    void Start()
    {
        NumberObjectTotal = _items.Count;
        gameUI.timeLeftLbl.text=TotalTime.ToString() + " sec";
        allSpawnPoints = new List<Vector2>();
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).tag=="SpawnPoint")
            {
                allSpawnPoints.Add(transform.GetChild(i).transform.position);
            }
            
        }
        BuildLevel();
    }

    private void BuildLevel()
    {
        
        switch (SceneManager.GetActiveScene().buildIndex)
        {
            case 1:
                percetageOfActiveMachines = 1;
                break;
            case 2:
                percetageOfActiveMachines = 4;
                break;
            case 3:
                percetageOfActiveMachines = 6;
                break;
            case 4:
                percetageOfActiveMachines = 6;
                break;
        }
        
        
        
        List<Vector2> allPointCopy = allSpawnPoints;
        for (int i = 0; i < percetageOfActiveMachines; i++)
        {
            GameObject newItem= Instantiate(_items[Random.Range(0, _items.Count)].gameObject);
            int randomPosition = Random.Range(0, allPointCopy.Count);
            newItem.transform.position=allPointCopy[randomPosition];
            newItem.GetComponent<Items>().boss = GetComponent<Environement>();
            allPointCopy.RemoveAt(randomPosition);
            _items.Add(newItem.GetComponent<Items>());
            newItem.GetComponent<Items>().ChangeState(0);
        }

        for (int i = percetageOfActiveMachines; i < NumberObjectTotal; i++)
        {
            GameObject newItem = Instantiate(_items[Random.Range(0, _items.Count)].gameObject);
            int randomPosition = Random.Range(0, allPointCopy.Count);
            newItem.transform.position = allPointCopy[randomPosition];
            newItem.GetComponent<Items>().boss = GetComponent<Environement>();
            allPointCopy.RemoveAt(randomPosition);
            _items.Add(newItem.GetComponent<Items>());
            newItem.GetComponent<Items>().ChangeState(2);
        }
        gameUI.Completion(NumberObjectFixed, NumberObjectTotal);
    }
    
    void StopTimer()
    {
        StopCoroutine(CourouTemps());
    }
    public void StartTimer()
    {
        StartCoroutine(CourouTemps());
    }
    IEnumerator CourouTemps()
    {

        for (int i = TotalTime; i > -1; i--)
        {
            gameUI.timeLeftLbl.text = i.ToString() + " sec";
            yield return new WaitForSeconds(1);
        }
        DidILoose();
        
    }

    public void DidILoose() {
        if (NumberObjectFixed<=NumberObjectTotal)
        {
            gameUI.GameOverPannel(0);
        }
    }
}
