﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    private GameObject pannel_Instructions;
    /// <summary>
    /// At the start of the level make sure the instructions pannel is deactivated
    /// </summary>
    private void Awake()
    {
        pannel_Instructions.SetActive(false);
    }
    /// <summary>
    /// Change the activation of the instuctions pannel
    /// </summary>
    public void OpenCloseInstruction() {
        pannel_Instructions.SetActive(!pannel_Instructions.activeSelf);
    }
    /// <summary>
    /// Launch the game with a determinated difficulty
    /// </summary>
    public void StartGame() {
        SceneManager.LoadScene(1);
    }
    /// <summary>
    /// Quit the game entirely
    /// </summary>
    public void Quit()
    {
        Application.Quit();
    }

}
