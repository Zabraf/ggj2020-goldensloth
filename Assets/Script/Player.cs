﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public bool isAlive;
    private string player_Name;
    [SerializeField]
    private int score;
    [SerializeField]
    private float radius;
    [SerializeField]
    private float speed;
    private float oldMovX;
    private float oldMovY;
    private float movX;
    private float movY;
    private Rigidbody2D rb;
    private Animator animator;
    bool isFixing;
    public bool TooFast;
    private UIGame ui;
    Items lastItem;

    bool isInInteractable;

    public bool IsFixing { get => isFixing; set => isFixing = value; }


    /// <summary>
    /// Make sure the first value are set at the default state
    /// </summary>
    private void Start()
    {
        isAlive = false;
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        ui = FindObjectOfType<UIGame>();
        TooFast = false;
        oldMovX = 0;
        oldMovY = 0;
        rb = GetComponent<Rigidbody2D>();
    }

    /// <summary>
    /// Verify the input 
    /// </summary>
    private void Update()
    {
        animator.SetBool("IsFixing", IsFixing);
        Movement();
        Repairing();
    }
    /// <summary>
    /// Take the input and from them do the given movement and animation
    /// </summary>
    public void Movement()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            oldMovX = -1;
            oldMovY = 0;
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            oldMovX = 1;
            oldMovY = 0;
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            oldMovX = 0;
            oldMovY = 1;
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            oldMovX = 0;
            oldMovY = -1;
        }
        animator.SetFloat("OldX", oldMovX);
        animator.SetFloat("OldY", oldMovY);


        animator.SetFloat("InputX", movX);
        animator.SetFloat("InputY", movY);
        if (movX > 0.5f || movX < -0.5f || movY > 0.5f || movY < -0.5f)
        {
            TooFast = true;
            
        }
        else
        {
            TooFast = false;
        }
        animator.SetBool("IsTooFast", TooFast);
        if ((Input.GetAxis("Horizontal") != 0) && !IsFixing || (Input.GetAxis("Vertical") != 0) && !IsFixing)
        {
            movX = Input.GetAxis("Horizontal");
            movY = Input.GetAxis("Vertical");
            if (Input.GetAxis("Fire3") != 0)
            {
                movX = movX / 2;
                movY = movY / 2;
            }
            rb.velocity = new Vector2(movX * speed, movY * speed);
        }
        else
        {
            movX = 0;
            movY = 0;
        }
    }

    public void Repairing()
    {
        if (Input.GetAxis("Fire1") != 0 && isInInteractable && lastItem != null)
        {
            IsFixing = true;
            //animator.SetTrigger("Paf");
            rb.velocity = new Vector2(0, 0);
            lastItem.Reparation();
        }
        else
        {

            if (lastItem != null)
            {
            isFixing = false;
                lastItem.StopReperation();
            }

        }

    }
    //To do
    public void Activate()
    {


    }
    /// <summary>
    /// In case the collision is with a Item
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Items>() != null)
        {
            isInInteractable = true;
            lastItem = collision.gameObject.GetComponent<Items>();


        };
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag=="Monster")
        {
            Die();
        }
    }
    /// <summary>
    /// In case the collision leave the item
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Items>() != null)
        {
            isInInteractable = false;
        }
    }

    /// <summary>
    /// 𝔻𝕀𝔼 𝔻𝕀𝔼 𝔻𝕀𝔼 𝔻𝕀𝔼
    /// 𝔻𝕀𝔼 𝔻𝕀𝔼 𝔻𝕀𝔼 𝔻𝕀𝔼
    /// 𝔻𝕀𝔼 𝔻𝕀𝔼 𝔻𝕀𝔼 𝔻𝕀𝔼
    /// 𝔻𝕀𝔼 𝔻𝕀𝔼 𝔻𝕀𝔼 𝔻𝕀𝔼
    /// </summary>
    public void Die()
    {
        ui.GameOverPannel(1);
    }

}
